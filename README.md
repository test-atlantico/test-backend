<h1>Mysql Credentials</h1>

<ul>
	<li>database: iatest</li>
	<li>username: iatest</li>
	<li>password: iatest</li>
</ul>

<h3>Create table of table "user"</h3>

create table `user` (
  `identity` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `created_date` date NOT NULL,
  `updated_date` date DEFAULT NULL,
  `email` varchar(200) NOT NULL,
  `admin` bit(1) NOT NULL,
  primary key (`identity`)
);

<h3>Observações</h3>

<p>Observações</p>