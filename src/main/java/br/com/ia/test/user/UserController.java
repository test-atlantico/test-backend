package br.com.ia.test.user;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class UserController {
	@Autowired
	UserRepository userRepository;

	@GetMapping("/user")
	public String userForm(Model model) {
		model.addAttribute("user", new UserEntity());
		model.addAttribute("userList", userRepository.findAll());

		return "user";
	}

	@PostMapping("/user/add")
	public String processForm(UserEntity user, BindingResult result) {
		if (result.hasErrors())
			return "user";

		user.setCreatedDate(new Date());

		this.userRepository.save(user);

		return "redirect:/user";
	}
}