package br.com.ia.test.user;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Controller;

@Controller
public interface UserRepository extends CrudRepository<UserEntity, Integer> {
	UserEntity findByIdentity(Integer identity);
}